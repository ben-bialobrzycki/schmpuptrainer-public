﻿
module myGame {

    export interface IUpdateable {
        update();
        isActive();
    }

    export interface IConfig {
        baseuri: string;
        apiKey: string;
    }
    export class GameManager implements IBackgroundable {

        DebugVersion = false;
        SoundOn = true;
        //CONTROLS
        static FIREKEY: number = 32; //spacebar
        static ENTER: number = 13;//enter
        static KEYF: number = 70;//enter

        static SCALE: number = 4;

        static getScale(): Phaser.Point {
            return new Phaser.Point(GameManager.SCALE, GameManager.SCALE);
        }
        width: number;
        height: number;
        currentLevel: number;
        spriteGroup: Phaser.Group;
        UILayerGroup: Phaser.Group;

        game: Phaser.Game;
        map: Phaser.Tilemap;
        layer: Phaser.TilemapLayer;
        player: Player;
        playerBulletGroup: Phaser.Group;
        enemyBulletGroup: Phaser.Group;
        enemyGroup: Phaser.Group;
        backgroundGroup: Phaser.Group;
        enemies: Array<Enemy>;
        updateables: Array<IUpdateable>;
        physics: Phaser.Physics;

        mouse: any;
        playerScore: number;
        playerScoreForLevel: number;
        exit: Phaser.Sprite;
        playerTargetSprite: Phaser.Sprite;

        playerTargetSet: boolean;
        playerTarget: Phaser.Point;
        playerStart: Phaser.Point;
        playerDead: boolean;

        totalPossibleScoreForLevel: number;
        totalPossibleScoreForAllLevel: number;
        simpleGame: GameManager;

        scoreText: Phaser.Text;
        scoreTextBottom: Phaser.Text;
        scoreTextPerc: Phaser.Text;
        playerLivesText: Phaser.Text;
        debugText: Phaser.Text;
        fpsText: Phaser.Text;
        levelClearText: Phaser.Text;
        levelClearSubText: Phaser.Text;
        levelClearSubText2: Phaser.Text;
        levelClearSubText3: Phaser.Text;
        gameOver: boolean;
        levelCleared: boolean;
        levelEventTimer: Phaser.Timer;


        styleScoreGood: any;
        stylePoint: any;
        styleScoreBad: any;
        backgroundScroller: BackgroundScroller;
        waveManager: WaveManager;

        constructor(game: Phaser.Game) {

            this.width = 800;
            this.height = 600;
            this.game = game;
        }

        playerSpeed: number = 150;

        calculateMirroredY(y) {
            return (this.height / 2) + ((this.height / 2) - y);
        }

        calculateMirroredX(x) {
            return -(x - (this.width / 2)) + (this.width / 2);
        }


        applyOffsetX(x, offsetX) {
            return x + offsetX * this.width;
        }

        applyOffsetY(y, offsetY) {
            return y + offsetY * this.height;
        }

        postHighScore(playerName: string, score: number) {

            var local_this = this;
            var xhr = new XMLHttpRequest();
            var uri = Config.getConfig().baseuri + "/score";
            xhr.open('POST', uri);
            xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');


            xhr.onload = function () {

                if (xhr.status !== 200) {
                    alert('Request failed.  Returned status of ' + xhr.status);
                }
                else {
                    local_this.postHighScoreDone();
                }
            };

            var data: IScore = {
                id: 0,
                scorePoints: score,
                scorePlayerName: playerName,
                createDate: new Date().toISOString(),
                scoreBoardAPIKey: Config.getConfig().apiKey
            }
            xhr.send(JSON.stringify(data));
        }

        postHighScoreDone() {
            this.showScoreSaver(false);
            this.levelClearSubText2.text = "Score Submitted";

        }

        static getHighScores(onSuccessCallback) {

            var xhr = new XMLHttpRequest();
            var config = Config.getConfig();
            var uri = config.baseuri + "/scoreboard/" + config.apiKey + "?t=" + new Date().getTime();
            xhr.open('GET', uri);
            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.onload = function () {
                if (xhr.status === 200) {
                    //   alert('Scores ' + xhr.responseText);
                    onSuccessCallback(xhr.responseText);
                }
                else {
                    alert('Request failed.  Returned status of ' + xhr.status);
                }
            };
            xhr.send();

        }


        showScoreSaver(show: boolean) {

            this.game.input.enabled = !show;

            var divArea = document.getElementById('divscoreInput');
            divArea.hidden = !show;


        }
        submitScore() {
            var txtElement: any = document.getElementById('txtschmupTrainerPlayerName');
            var playerName: string = txtElement.value;
            playerName = playerName.trim();
            if (playerName.trim().length == 0) {
                alert("Please Fill in a Name");
                return;
            }

            this.postHighScore(playerName, this.playerScore);

        }

        skipUpload() {
            this.showScoreSaver(false);
            this.levelClearSubText2.text = "Score Not Submitted";
        }
        addScoreEnterer() {
            this.showScoreSaver(true);
        }
        showGameOver() {
            this.fadeInText(this.levelClearText);

            this.fadeInTextArray([this.levelClearText, this.levelClearSubText, this.levelClearSubText2, this.levelClearSubText3]);
            this.levelClearText.text = "Game Over";
            this.levelClearSubText.text = "Failed To Qualify To Next Round";
            this.levelClearSubText2.visible = this.levelClearSubText3.visible = true;
            this.levelClearSubText2.text = "Submit Score " + this.playerScore;
            this.levelClearSubText3.text = "Space to Restart, Enter to go back to Main Screen\nExit Full Screen to see score submission button";
            this.addScoreEnterer();
        }

        fadeInText(text: Phaser.Text) {
            text.alpha = 0.1;
            this.game.add.tween(text).to({ alpha: 1 }, 700, 'Linear', true);
        }

        fadeInTextArray(text: Array<Phaser.Text>) {
            for (var i = 0; i < text.length; i++) {
                this.fadeInText(text[i]);
            }

        }


        fadeOutText(text: Phaser.Text) {
            text.alpha = 1.0;
            this.game.add.tween(text).to({ alpha: 0 }, 700, 'Linear', true);
            this.game.time.create().add(700, () => text.visible = false);
        }

        fadeOutTextArray(text: Array<Phaser.Text>) {
            for (var i = 0; i < text.length; i++) {
                this.fadeOutText(text[i]);
            }

        }


        showGameComplete() {
            this.fadeInTextArray([this.levelClearText, this.levelClearSubText, this.levelClearSubText2, this.levelClearSubText3]);
            this.levelClearText.text = "All Levels Complete";
            this.levelClearSubText.text = "Congratulations";
            this.levelClearSubText2.visible = this.levelClearSubText3.visible = true;
            this.levelClearSubText2.text = "Submit Score " + this.playerScore;
            this.levelClearSubText3.text = "Space to Restart, Enter to go back to Main Screen\nExit Full Screen to see score submission button";
            this.addScoreEnterer();
        }


        levelRequiredPercentage: number = 0.5;
        maxLevel = 5;
        debugAllowLowScore = false;
        showLevelClear() {
            this.fadeInTextArray([this.levelClearText, this.levelClearSubText]);
            this.levelClearText.visible = true;
            this.levelClearSubText.visible = true;
            this.levelClearSubText.text = "Level Score " + this.playerScoreForLevel + " Out of Maximum " + this.totalPossibleScoreForLevel;
            if (this.currentLevel >= this.maxLevel) {
                var timer = this.game.time.create();
                timer.add(2000, () => { this.showGameComplete(); })
                this.gameOver = true;
                timer.start();
            }
            else if ((this.playerScoreForLevel < this.levelRequiredPercentage * this.totalPossibleScoreForLevel) && !this.debugAllowLowScore) {
                var timer = this.game.time.create();
                timer.add(2000, () => { this.showGameOver(); })
                this.gameOver = true;
                timer.start();
            }
            else {
                this.levelIntroPlaying = true;
                var timer = this.game.time.create();
                timer.add(3500, () => { this.setForLevel(this.currentLevel + 1) });
                timer.start();
            }

        }

        runGameOver() {
            var timer = this.game.time.create();
            timer.add(2000, () => { this.showGameOver(); })
            this.gameOver = true;
            timer.start();
        }
        checkGameOver() {

            if (this.gameOver)
                return;
            if (this.levelCleared)
                return;
            if (this.waveManager.currentWave <= 0 && this.enemies.length == 0 && this.levelEventTimer.events.length == 0) {
                this.levelCleared = true;

                var timer = this.game.time.create();
                timer.add(2000, () => this.showLevelClear());
                timer.start();

            }

        }

        removeEnemy(enemy: Enemy) {
            for (var i = 0; i < this.enemies.length; i++) {
                if (this.enemies[i] == enemy) {
                    this.enemies.splice(i, 1);
                    break;
                }
            }
        }
        bulletEnemyCollision(bulletSprite: Phaser.Sprite, enemySprite: Phaser.Sprite) {
            var bullet: PlayerBullet = bulletSprite.data;
            var enemy: Enemy = enemySprite.data;

            if (enemy.isDead())
                return;
            bulletSprite.kill();
            enemy.takeDamage(bullet.damageValue);

            if (enemy.isDead()) {
                this.updateScore(enemy.scoreBonus);
                enemy.destroy(true);
                this.addExplosion(enemySprite.position);
                this.setNewPointText(enemySprite.position, enemy.scoreBonus, false);
                if (enemy.wave) {
                    enemy.wave.waveEnemyCount--;
                    if (enemy.wave.waveEnemyCount == 0) {
                        this.setNewPointText(enemySprite.position, enemy.wave.waveBonus, true);
                    }
                }
            }


        }

        getInputForGameOverScreen() {
            this.player.StopMovement();
            if (this.game.input.keyboard.isDown(GameManager.FIREKEY))
                this.ResetGame();
            if (this.game.input.keyboard.isDown(GameManager.ENTER)) {
                this.showScoreSaver(false);
                if (!this.mutemusic)
                    this.mainMusic.stop();
                this.game.state.start("MainMenu");
            }

        }


        static formatPoint(point: Phaser.Point): string {
            return "(" + point.x.toFixed(2) + ", " + point.y.toFixed(2) + ")";
        }

        toggleAudioKey: Phaser.Key = null;
        fireWaveKey: Phaser.Key = null;

        toggleAudio() {
            if (this.mainMusic.isPlaying)
                this.mainMusic.pause();
            else// if (!this.mainMusic.isPlaying)
                this.mainMusic.resume();

        }

        togglePause() {
            this.game.paused = !this.game.paused;
        }

        doDebugstuff() {

            if (this.fpsText.visible) {
                this.fpsText.text = this.game.time.fps.toFixed(2);;
            }
        }



        update() {

            this.doDebugstuff();

            if (this.player.isAlive() && !this.gameOver) {
                this.player.update();
            }

            if (this.gameOver) {
                this.getInputForGameOverScreen();
            }

            if (!this.DebugVersion) {
                // if (!this.levelIntroPlaying)
                //     this.waveManager.doWaves();
            }

            this.updateEnemies();
            this.updateUpdateables();
            this.clearDeadOrOutOfBounds();
            if (this.player.playerLives > 0 && this.player.isAlive()) {
                this.game.physics.arcade.overlap(this.player.sprite, this.enemyGroup, (obj1: any, obj2: any) => this.player.playerEnemyCollision(obj1, obj2));
                this.game.physics.arcade.overlap(this.player.sprite, this.enemyBulletGroup, (obj1: any, obj2: any) => this.player.playerEnemyBulletCollision(obj1, obj2));
            }
            this.game.physics.arcade.overlap(this.playerBulletGroup, this.enemyGroup, (obj1: any, obj2: any) => this.bulletEnemyCollision(obj1, obj2));
            this.checkGameOver();
        }

        updatePlayerLivesText() {
            this.playerLivesText.text = "Lives: " + this.player.playerLives;
        }

        amendTotalPossibleScoreForEnemy(enemy: Enemy) {
            this.totalPossibleScoreForLevel += enemy.scoreBonus - this.player.bulletCost;
            this.setScoreText();
        }

        amendTotalPossibleScoreForWave(wave: Wave) {
            this.totalPossibleScoreForLevel += wave.waveBonus;
            this.setScoreText();
        }


        updateEnemies() {
            for (var i = 0; i < this.enemies.length; i++) {
                this.enemies[i].update();
            }
        }


        outOfBounds(sprite: Phaser.Sprite): boolean {
            var limit = this.width * 0.2;
            if (sprite.position.x < -limit || sprite.position.x > this.width + limit)
                return true;

            if (sprite.position.y < -limit || sprite.position.y > this.height + limit)
                return true;

            return false;
        }

        clearDeadOrOutOfBounds() {

            var dead = new Array<Enemy>();
            for (var i = 0; i < this.enemies.length; i++) {
                if (this.enemies[i].doNotRemove)
                    continue;

                if (this.outOfBounds(this.enemies[i].sprite) || !this.enemies[i].alive || !this.enemies[i].sprite.alive) {
                    dead.push(this.enemies[i]);
                }
            }

            for (var i = 0; i < dead.length; i++)
                dead[i].destroy(true);
        }

        updateUpdateables() {
            for (var i = 0; i < this.updateables.length; i++) {
                this.updateables[i].update();
            }
        }

        updateScore(change: number) {
            this.playerScore += change;
            this.playerScoreForLevel += change;
            this.setScoreText();
        }

        showHideFPS() {
            this.fpsText.visible = !this.fpsText.visible;
        }

        addToUpdateables(updateable: IUpdateable) {
            this.updateables.push(updateable);

        }

        removeAllSprites() {
            for (var i = 0; i < this.enemies.length; i++)
                this.enemies[i].destroy(false);
            this.enemies = new Array<Enemy>();
            this.enemyBulletGroup.removeAll(true);
            this.playerBulletGroup.removeAll(true);
        }

        ResetGame() {
            this.gameOver = false;
            this.showScoreSaver(false);

            this.resetPlayerForNewGame();
            this.removeAllSprites();
            this.gameOver = false;
            this.levelEventTimer.clearPendingEvents();
            this.levelIntroPlaying = true;
            this.setForLevel(1);
            this.setScoreText();
            this.updatePlayerLivesText();

        }

        mutemusic: boolean = false;

        resetMusic() {
            if (!this.mutemusic) {
                this.mainMusic.loop = true;
                if (!this.DebugVersion && this.SoundOn && !this.mainMusic.isPlaying)
                    this.mainMusic.play();
            }
        }
        setForLevel(level: number) {
            this.levelClearText.visible = false;
            this.levelClearSubText.visible = false;
            this.levelClearSubText2.visible = false;
            this.levelClearSubText3.visible = false;

            this.debugText.visible = false;
            this.playerScoreForLevel = 0;
            if (level == 1) {

                this.resetMusic();
                this.playerScore = 0;
                this.totalPossibleScoreForAllLevel = 0;
                this.levelRequiredPercentage = 0.3;
                this.waveManager.waveTimeInterval = 8000;
                this.gameOver = false;
                this.showScoreSaver(false);
            }

            if (level == 2) {
                this.levelRequiredPercentage = 0.4;
            }

            if (level == 3) {
                this.levelRequiredPercentage = 0.5;
            }

            if (level == 4) {
                this.levelRequiredPercentage = 0.6;
            }
            if (level == 5) {
                this.levelRequiredPercentage = 0.7;
            }

            this.levelCleared = false;

            this.totalPossibleScoreForLevel = 0;
            this.currentLevel = level;

            this.waveManager.setWavesForLevel();

            this.levelEventTimer.add(this.levelIntroTime, () => this.finishLevelIntro())

            this.fadeInTextArray([this.levelClearText, this.levelClearSubText]);

            this.levelClearText.visible = true;
            this.levelClearSubText.visible = true;
            this.levelClearText.text = "Level " + this.currentLevel;
            if (this.currentLevel == this.maxLevel)
                this.levelClearSubText.text = "Final Level";
            else
                this.levelClearSubText.text = "Need Percentage >  " + 100 * (this.levelRequiredPercentage) + "%";
        }

        levelIntroTime = 3000;
        levelIntroPlaying = true;
        finishLevelIntro() {
            this.fadeOutTextArray([this.levelClearText, this.levelClearSubText]);
            this.levelIntroPlaying = false;
        }
        getPlayerPosition() {
            return new Phaser.Point(this.player.body.position.x, this.player.body.position.y);
        }

        currentPointTextIndex = 0;
        pointTextCollection: Array<Phaser.Text>;
        pointTextVisible = 500;
        pointYOffset = 20;

        public setNewPointText(point: Phaser.Point, score: number, isBonus: boolean) {
            var text = this.pointTextCollection[this.currentPointTextIndex];
            this.currentPointTextIndex++;
            if (this.currentPointTextIndex >= this.pointTextCollection.length)
                this.currentPointTextIndex = 0;

            text.position.x = point.x;
            text.position.y = point.y - this.pointYOffset;

            var newPos;
            if (isBonus) {
                newPos = point.y - (3 * this.pointYOffset);
                text.colors[0] = '#ccc918';
                text.text = "Wave Bonus: " + score.toString();
            }
            else {
                newPos = point.y - (2 * this.pointYOffset);
                text.colors[0] = '#91b6f2';
                text.text = score.toString();
            }
            text.visible = true;
            text.alpha = 1;

            this.game.add.tween(text).to({ alpha: 0 }, 700, 'Linear', true);
            this.game.add.tween(text.position).to({ y: newPos }, 700, 'Linear', true);
            this.levelEventTimer.add(this.pointTextVisible, () => { text.visible = false; });

        }


        resetPlayerForNewGame() {
            this.player.resetForNewGame();
        }

        create() {
            this.game.input.gamepad.start();
            this.levelEventTimer = this.game.time.create(false);
            this.levelEventTimer.start();
            this.playerTarget = new Phaser.Point(0, 0);
            this.playerTargetSet = false;
            this.waveManager = new WaveManager(this);
            this.waveManager.startWaves();
            this.enemies = new Array<Enemy>();
            this.updateables = new Array<IUpdateable>();
            this.currentLevel = 1;
            this.levelIntroPlaying = true;
            console.log("Create Called");
            this.game.stage.backgroundColor = '#111111';

            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.renderer.renderSession.roundPixels = false;
            this.game.physics.arcade.gravity.y = 0;
            // this.resetPlayerForNewGame();

            this.player = new Player(this, this.game);
            this.playerBulletGroup = this.game.add.group();
            this.enemyBulletGroup = this.game.add.group();
            this.enemyGroup = this.game.add.group();
            this.backgroundGroup = this.game.add.group();


            this.styleScoreGood = { font: "20px 'Lucida Console', Monaco, monospace", fill: "#56bc6c", align: "left" };
            this.stylePoint = { font: "20px 'Lucida Console', Monaco, monospace", fill: "#91b6f2", align: "left" };
            this.styleScoreBad = { font: "20px 'Lucida Console', Monaco, monospace", fill: "#af3858", align: "left" };

            var style = { font: "20px 'Lucida Console', Monaco, monospace", fill: "#ff0044", align: "left", boundsAlignH: "center", boundsAlignV: "middle" };
            var styleLevelText = { font: "60px 'Lucida Console', Monaco, monospace", fill: "#4f9e62", align: "centre", boundsAlignH: "center", boundsAlignV: "middle" };
            var styleLevelTextSub = { font: "30px 'Lucida Console', Monaco, monospace", fill: "#4f9e62", align: "centre", boundsAlignH: "center", boundsAlignV: "middle" };
            var styleLevelTextSmall = { font: "15px 'Lucida Console', Monaco, monospace", fill: "#4f9e62", align: "centre", boundsAlignH: "center", boundsAlignV: "middle" };
            //  var styleLevelTextSub2 = { font: "40px 'Lucida Console', Monaco, monospace", fill: "#4f9e62", align: "centre" };


            this.scoreText = this.game.add.text(0, 0, "Lives", this.styleScoreGood);

            this.scoreTextBottom = this.game.add.text(0, 0, "Lives", this.styleScoreGood);
            this.scoreTextPerc = this.game.add.text(0, 0, "Lives", this.styleScoreGood);
            this.playerLivesText = this.game.add.text(0, 0, "Lives", this.styleScoreGood);

            this.scoreText.fixedToCamera = true;
            this.scoreTextBottom.fixedToCamera = true;
            this.scoreTextPerc.fixedToCamera = true;
            // this.scoreText.anchor.set(0.5);

            this.scoreText.setTextBounds(10, 20, 50, 20);
            this.scoreTextBottom.setTextBounds(10, 50, 50, 20);
            this.playerLivesText.setTextBounds(this.width - 150, 20, 50, 20);
            this.scoreTextPerc.setTextBounds(this.width - 150, 40, 50, 20);


            this.debugText = this.game.add.text(500, 80, "Lives", this.styleScoreGood);
            this.debugText.fixedToCamera = true;
            this.fpsText = this.game.add.text(500, 100, "FPS", this.styleScoreGood);
            this.fpsText.fixedToCamera = true;
            this.fpsText.visible = false;

            this.levelClearText = this.game.add.text(0, 0, "Level Clear", styleLevelText);
            this.levelClearSubText = this.game.add.text(0, 0, "Level Clear___  Out Of Maximum of ___", styleLevelTextSub);
            this.levelClearSubText2 = this.game.add.text(0, 0, "Score ", styleLevelTextSub);
            this.levelClearSubText3 = this.game.add.text(0, 0, "Score ", styleLevelTextSmall);


            this.setTextDefaults(this.levelClearText, this.height * 0.2);
            this.setTextDefaults(this.levelClearSubText, this.height * 0.3);
            this.setTextDefaults(this.levelClearSubText2, this.height * 0.4);
            this.setTextDefaults(this.levelClearSubText3, this.height * 0.5);

            this.pointTextCollection = new Array<Phaser.Text>();
            for (var i = 0; i < 15; i++) {
                var pointText = this.game.add.text(0, 0, "Lives", this.stylePoint);
                pointText.visible = false;
                this.pointTextCollection.push(pointText);
            }

            if (!this.mutemusic)
                this.mainMusic = this.game.add.audio("MainMusic");

            if (!this.DebugVersion)
                this.setForLevel(1);

            this.setScoreText();
            this.updatePlayerLivesText();
            this.backgroundScroller = new BackgroundScroller(this, this.game);
            this.backgroundScroller.initialiseBackround();

            GameManager.addFullScreenButton(this.game);

            this.toggleAudioKey = this.game.input.keyboard.addKey('M'.charCodeAt(0));
            this.toggleAudioKey.onDown.add(() => this.toggleAudio());
            this.game.input.keyboard.addKey('P'.charCodeAt(0)).onDown.add(() => this.togglePause());



            //DEBUG STUFF
            if (this.DebugVersion) {
                this.game.time.advancedTiming = true;
                this.game.input.keyboard.addKey('0'.charCodeAt(0)).onDown.add(() => this.waveManager.addLinearWave());
                this.game.input.keyboard.addKey('1'.charCodeAt(0)).onDown.add(() => this.setForLevel(1));
                this.game.input.keyboard.addKey('2'.charCodeAt(0)).onDown.add(() => this.setForLevel(2));
                this.game.input.keyboard.addKey('3'.charCodeAt(0)).onDown.add(() => this.setForLevel(3));
                this.game.input.keyboard.addKey('4'.charCodeAt(0)).onDown.add(() => this.setForLevel(4));
                this.game.input.keyboard.addKey('5'.charCodeAt(0)).onDown.add(() => this.setForLevel(5));

                this.game.input.keyboard.addKey('J'.charCodeAt(0)).onDown.add(() => this.postHighScore('testplayer1', 1000));

                //    this.game.input.keyboard.addKey('2'.charCodeAt(0)).onDown.add(() => this.waveManager.addGreenEnemySinus());
                //     this.game.input.keyboard.addKey('F'.charCodeAt(0)).onDown.add(() => this.waveManager.addBezierWave());
                //     this.game.input.keyboard.addKey('I'.charCodeAt(0)).onDown.add(() => this.waveManager.addGreenEnemy());
                //    this.game.input.keyboard.addKey('U'.charCodeAt(0)).onDown.add(() => this.waveManager.addPurpleEnemy());
                this.game.input.keyboard.addKey('Y'.charCodeAt(0)).onDown.add(() => this.waveManager.addWave());
            }
        }



        mainMusic: Phaser.Sound;
        setTextDefaults(text: Phaser.Text, yPos: number) {
            text.fixedToCamera = true;
            text.setTextBounds(0, yPos, this.width, 100);
        }


        setScoreText() {
            var perc;
            if (this.totalPossibleScoreForLevel == 0)
                perc = 0
            else
                perc = this.playerScoreForLevel / this.totalPossibleScoreForLevel;

            this.scoreText.text = "Total Score: " + this.playerScore;
            this.scoreTextBottom.text = "Level Score: " + this.playerScoreForLevel;
            this.scoreTextPerc.text = "Perc " + (perc * 100).toFixed(2) + "% ";

            if (perc > this.levelRequiredPercentage)
                this.scoreTextPerc.setStyle(this.styleScoreGood);
            else
                this.scoreTextPerc.setStyle(this.styleScoreBad);
        }

        setDebugText(text) {
            if (!this.debugText.visible)
                return;
            this.debugText.text = "Debug: " + text;

        }
        preload() {
            //this.gameManager = new GameManager(this);
            console.log("Preload called");

            this.game.load.spritesheet('EnemyBullet', 'assets/EnemyBullet.png', 4, 4);
            this.game.load.spritesheet('ship', 'assets/ship.png', 8, 8);
            this.game.load.spritesheet('explosion', 'assets/explosion.png', 8, 8);
            this.game.load.spritesheet('alien1', 'assets/spacealien1.png', 8, 8);
            this.game.load.spritesheet('alien2', 'assets/Alien2.png', 8, 8);
            this.game.load.spritesheet('alien3', 'assets/alien3.png', 8, 8);
            this.game.load.spritesheet('alienBig', 'assets/alienBig.png', 16, 16);
            this.game.load.spritesheet('playerBullet', 'assets/Bullet.png', 4, 4);
            this.game.load.image('FullScreenButton', 'assets/FullScreenButton.png');
            this.game.load.image('star1', 'assets/star1.png');
            this.game.load.image('star2', 'assets/star2.png');
            this.game.load.image('galaxy', 'assets/galaxy.png');

            this.game.load.audio('PlayerShoot', 'assets/sounds/lasershoot.mp3');

            this.game.load.audio('Explosion', 'assets/sounds/explosion.mp3');

            this.game.load.audio('EnemyShoot', 'assets/sounds/enemyshoot.mp3');

            //this.game.load.audio('MainMusic', 'assets/sounds/Space4eqdcool.mp3');
            this.game.load.audio('MainMusic', 'assets/sounds/BrassyBass.mp3');

            var _this = this;

        }

        WebFontConfig: any;
        addExplosion(position: Phaser.Point) {
            var explosion: Explosion = new Explosion(this);
            explosion.setPosition(position);

        }

        static addFullScreenButton(game: Phaser.Game) {
            var buttonFullScreen = game.add.button(750, 550, 'FullScreenButton', () => GameManager.gofull(game));
            buttonFullScreen.scale = GameManager.getScale();
        }

        static gofull(game: Phaser.Game) {

            if (game.scale.isFullScreen) {
                game.scale.stopFullScreen();
            }
            else {
                game.scale.startFullScreen(false);
            }

        }
    }

}