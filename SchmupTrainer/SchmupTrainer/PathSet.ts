﻿module myGame {
    export class PathSet {

        XPoints: Array<number>;
        YPoints: Array<number>;
        curveTime = 4000;

        constructor() {
            this.XPoints = new Array<number>();
            this.YPoints = new Array<number>();
        }


        static generateLinearSet(gameManager: GameManager, pointLength, start: Phaser.Point, includeEnd: boolean) {
            var ps: PathSet = new PathSet();


            for (var i = 0; i < pointLength; i++) {
                if (i == 0) {
                    if (start != null) {
                        ps.YPoints.push(start.y);
                        ps.XPoints.push(start.x);
                    }
                    else {
                        ps.YPoints.push(-5);
                        ps.XPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.width));
                    }
                }
                else if (i == pointLength - 1 && includeEnd) {
                    ps.YPoints.push(gameManager.height + 200);
                    ps.XPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.width));
                }
                else {
                    ps.YPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.height * 0.8)); //stay in top 70% of screen
                    ps.XPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.width));
                }


            }
            return ps;

        }

        static generateRandomForPointCount(gameManager: GameManager, pointLength, start: Phaser.Point, includeEnd: boolean) {
            var ps: PathSet = new PathSet();


            for (var i = 0; i < pointLength; i++) {
                if (i == 0) {
                    if (start != null) {
                        ps.YPoints.push(start.y);
                        ps.XPoints.push(start.x);
                    }
                    else {
                        ps.YPoints.push(-5);
                        ps.XPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.width));
                    }
                }
                else if (i == pointLength - 1 && includeEnd) {
                    ps.YPoints.push(gameManager.height + 200);
                    ps.XPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.width));
                }
                else {
                    ps.YPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.height * 0.8)); //stay in top 70% of screen
                    ps.XPoints.push(gameManager.game.rnd.integerInRange(0, gameManager.width));
                }


            }
            return ps;

        }

        static generateRandom(gameManager: GameManager) {
            return PathSet.generateRandomForPointCount(gameManager, 5, null, true);

        }

        static generateRandomLong(gameManager: GameManager, includeEnd: boolean) {
            return PathSet.generateRandomForPointCount(gameManager, 10, null,includeEnd);

        }

       
    }
}