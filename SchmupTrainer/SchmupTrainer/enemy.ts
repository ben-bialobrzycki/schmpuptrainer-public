﻿
module myGame {

    export enum EnemyType { blue, Green, Yellow, BigPurple }
    export enum MovementType { bezier, preset, Sinusoidal };
    export class Enemy {

        movmentType: MovementType = MovementType.bezier;
        sprite: Phaser.Sprite;
        target: Phaser.Point;
        targetSet: boolean;
        game: Phaser.Game;
        gameManager: GameManager;
        speed: number;
        alive: boolean;
        group: Phaser.Group;
        body: Phaser.Physics.Arcade.Body;
        patrolTargets: Array<Phaser.Point>;
        patrolIndex: number;

        shootSound: Phaser.Sound;
        static RandomXSpeed = 30;
        static RandomYSpeedMin = 50;
        static RandomYSpeedMax = 150;

        playerCollisionScoreCost = 0;//150;//using lives mechanic instead
        health = 1;
        scoreBonus = 100;
        enemyType: EnemyType;

        shotOnce = false;
        shootResetInitialMin = 1000;
        shootResetInitialTimeMax = 4000;

        shootResetTimeMin = 1000;
        shootResetTimeMax = 4000;
        velocity = 250;
        velocitySinusCentre = 0.08;
        doNotRemove: boolean;
        bulletSpeed = 150;
        aimRandomOffset = 100;
        chanceToShoot = 1.0;
        wave: Wave;

        constructor(gameManager: GameManager, game: Phaser.Game, enemyType: EnemyType) {

            this.doNotRemove = false;
            this.enemyType = enemyType;
            this.gameManager = gameManager;
            this.game = game;
            this.setParametersForType();
            this.shootSound = this.game.add.audio('EnemyShoot');
            this.sprite.scale.setTo(4, 4);
            this.game.physics.arcade.enable(this.sprite);
            this.gameManager.enemyGroup.add(this.sprite);
            this.gameManager.enemies.push(this);
            this.speed = 70;
            this.alive = true;
            this.targetSet = false;

            this.sprite.animations.add('move', [0, 1], 10, true);
            this.sprite.data = this;
            this.body = this.sprite.body;
            this.sprite.blendMode = PIXI.blendModes.NORMAL;
            this.sprite.animations.play("move");

        }

        setParametersForType() {

            this.shootResetInitialMin = 500;
            if (this.enemyType == EnemyType.blue) {
                this.sprite = this.game.add.sprite(0, 0, 'alien1');
                this.shootResetInitialMin = 750;
                this.shootResetTimeMin = 3500;
                this.chanceToShoot = 0.5;
            }
            else if (this.enemyType == EnemyType.Green) {
                this.sprite = this.game.add.sprite(0, 0, 'alien2');
                this.scoreBonus = 600;
                this.shootResetTimeMin = 1500;
                this.health = 2;
                this.phases = 3;
            }
            else if (this.enemyType == EnemyType.Yellow) {
                this.sprite = this.game.add.sprite(0, 0, 'alien3');
                this.shootResetTimeMin = 2000;
                this.scoreBonus = 300;
            }
            else if (this.enemyType == EnemyType.BigPurple) {
                this.sprite = this.game.add.sprite(0, 0, 'alienBig');
                this.scoreBonus = 1000;
                this.shootResetTimeMin = 1500;
                this.health = 4;
                this.phases = 6;
            }

            this.shootResetInitialTimeMax = this.shootResetTimeMax;
        }

        flashTime = 300;
        takeDamage(dmg) {
            this.health -= dmg;
            this.sprite.tint = 0xff00ff;
            this.timer.add(this.flashTime, () => { this.resetTint(); })
        }

        resetTint() {
            this.sprite.tint = 0xffffff;
        }
        isDead() {
            return this.health <= 0;
        }
        setPosition(position: Phaser.Point) {
            this.sprite.position.x = position.x;
            this.sprite.position.y = position.y;

        }

        setVelocity(velocity: Phaser.Point) {
            this.body.velocity.x = velocity.x;
            this.body.velocity.y = velocity.y;
        }


        patrolXLimit = 200;
        patrolYLimit = 400;

        pathPointsx: Array<Array<number>>;
        pathPointsy: Array<Array<number>>;
        curveTime = 4000.0;
        currentPerc = 0;
        pathCurrentTime = 0;
        shootOnReset: boolean;
        startedCurveMovement = false;
        phases: number = 1;

        update() {

            this.tryShoot();

            if (this.movmentType == MovementType.bezier)
                this.dobezierMovement();
            else if (this.movmentType == MovementType.preset) {
                this.doPresetMovement();
            }
            else if (this.movmentType == MovementType.Sinusoidal) {
                this.doSinusoidalMovement();
            }


        }

        presetPath: PresetPath;
        presetPathOptions: PresetPathOptions;
        currentFragmentIndex: number;
        currentSubFragmentIndex: number;

        setPresetPath(presetPath: PresetPath, presetPathOptions: PresetPathOptions) {
            this.presetPathOptions = presetPathOptions;
            this.movmentType = MovementType.preset;
            this.presetPath = presetPath;
            this.currentFragmentIndex = 0;
            this.currentSubFragmentIndex = 0;
            this.pathCurrentTime = 0;
            var firstPart = this.presetPath.parts[0];
            var start = new Phaser.Point(firstPart.points[0].x, firstPart.points[0].y);
            if (this.presetPathOptions.mirrorX)
                start.x = this.gameManager.calculateMirroredX(start.x);
            if (this.presetPathOptions.mirrorY)
                start.y = this.gameManager.calculateMirroredY(start.y);
            start.x = this.gameManager.applyOffsetX(start.x, presetPathOptions.offset.x);
            start.y = this.gameManager.applyOffsetY(start.y, presetPathOptions.offset.y);
            this.setPosition(start);

        }

        currentAngle: number;

        getAngularOffset() {
            var x = Math.cos(this.currentAngle) * this.presetPathOptions.radius;
            var y = Math.sin(this.currentAngle) * this.presetPathOptions.radius;
            return new Phaser.Point(x, y);
        }
        setSinusoidalPath(presetPath: PresetPath, presetPathOptions: PresetPathOptions) {
            this.doNotRemove = true;
            this.presetPathOptions = presetPathOptions;
            this.movmentType = MovementType.Sinusoidal;
            this.presetPath = presetPath;
            this.currentFragmentIndex = 0;
            this.currentSubFragmentIndex = 0;
            this.pathCurrentTime = 0;
            var firstPart = this.presetPath.parts[0];
            var start = new Phaser.Point(firstPart.points[0].x, firstPart.points[0].y);
            if (this.presetPathOptions.mirrorX)
                start.x = this.gameManager.calculateMirroredX(start.x);
            if (this.presetPathOptions.mirrorY)
                start.y = this.gameManager.calculateMirroredY(start.y);

            this.sinusCentrePoint = new Phaser.Point(start.x, start.y);

            this.currentAngle = 0;
            var offset = this.getAngularOffset();
            start.x += offset.x;
            start.y += offset.y;

            this.sprite.position.x = start.x;
            this.sprite.position.y = start.y;
        }


        closeEnough = 5;
        distancePointToPointSquared(sx, sy, x, y) {
            var xdist = (sx - x);
            var ydist = (sy - y);
            return (xdist * xdist) + (ydist * ydist);

        }

        distanceToPointSquared(x, y) {
            return this.distancePointToPointSquared(this.body.position.x, this.body.position.y, x, y);
        }


        sinusCentrePoint: Phaser.Point;
        doSinusoidalMovement() {
            var target = this.getCurrentTarget();

            if (this.distancePointToPointSquared(this.sinusCentrePoint.x, this.sinusCentrePoint.y, target.x, target.y) < this.closeEnough) {
                this.setNextFragment(true);
            }

            this.currentAngle += this.game.time.elapsedMS * this.presetPathOptions.angularVelocity / Phaser.Math.PI2;
            var vec = new Phaser.Point(target.x - this.sinusCentrePoint.x, target.y - this.sinusCentrePoint.y);
            vec.normalize();
            this.sinusCentrePoint.x += vec.x * this.velocitySinusCentre * this.game.time.elapsedMS;
            this.sinusCentrePoint.y += vec.y * this.velocitySinusCentre * this.game.time.elapsedMS;
            var offset = this.getAngularOffset();
            var position = new Phaser.Point(this.sinusCentrePoint.x, this.sinusCentrePoint.y);
            position.x += offset.x;
            position.y += offset.y;
            this.sprite.x = position.x;
            this.sprite.y = position.y;

        }

        getCurrentTarget() {
            if (this.currentFragmentIndex >= this.presetPath.parts.length)
                return; //we are done should have been removed by now
            var curFrag = this.presetPath.parts[this.currentFragmentIndex];
            if (this.currentSubFragmentIndex >= curFrag.points.length)
                this.currentSubFragmentIndex = 0;
            var x = curFrag.points[this.currentSubFragmentIndex].x;
            var y = curFrag.points[this.currentSubFragmentIndex].y;
            if (this.presetPathOptions.mirrorX)
                x = this.gameManager.calculateMirroredX(x);
            if (this.presetPathOptions.mirrorY) {
                y = this.gameManager.calculateMirroredY(y);
            }
            x = x + (this.presetPathOptions.offset.x * this.gameManager.width);
            y = y + (this.presetPathOptions.offset.y * this.gameManager.height);
            return new Phaser.Point(x, y);
        }

        setNextFragment(loop: boolean) {
            var curFrag = this.presetPath.parts[this.currentFragmentIndex];
            if (this.currentSubFragmentIndex < curFrag.points.length)
                this.currentSubFragmentIndex++;
            else {
                this.currentSubFragmentIndex = 0;
                this.currentFragmentIndex++;
            }
            if (this.currentFragmentIndex >= this.presetPath.parts.length && loop) {
                this.currentFragmentIndex = 0;
                this.currentSubFragmentIndex = 0;
            }
        }

        doPresetMovement() {

            var target = this.getCurrentTarget();
            if (this.distanceToPointSquared(target.x, target.y) < this.closeEnough) {
                this.setNextFragment(false)
            }
            this.game.physics.arcade.moveToXY(this.sprite, target.x, target.y, this.velocity, );

        }

        dobezierMovement() {
            if (this.startedCurveMovement) {
                this.pathCurrentTime += this.game.time.physicsElapsedMS;

                this.currentPerc = Phaser.Math.clamp((this.pathCurrentTime / this.curveTime), 0, 1.0);
                if (this.currentPerc >= 1.0) {
                    if (this.currentSubFragmentIndex >= this.pathPointsx.length - 1) {
                        this.destroy(true);
                        return;
                    }
                    this.pathCurrentTime = 0;
                    this.currentSubFragmentIndex++;
                    this.currentPerc = 0;
                }
                var xpos = Phaser.Math.bezierInterpolation(this.pathPointsx[this.currentSubFragmentIndex], this.currentPerc);
                var ypos = Phaser.Math.bezierInterpolation(this.pathPointsy[this.currentSubFragmentIndex], this.currentPerc);
                this.setPosition(new Phaser.Point(xpos, ypos));
            }

        }

        timer: Phaser.Timer;

        tryShoot() {
            if (!this.shootOnReset) {
                this.shootOnReset = true;
                this.timer = this.game.time.create();
                if (!this.shotOnce) {
                    this.timer.add(this.game.rnd.between(this.shootResetInitialMin, this.shootResetInitialTimeMax), () => { this.doShoot(); });
                    this.shotOnce = true;
                }
                else
                    this.timer.add(this.game.rnd.between(this.shootResetTimeMin, this.shootResetTimeMax), () => { this.doShoot(); });
                this.timer.start();
            }
        }

        doShoot() {

            this.shootOnReset = false;
            if (this.game.rnd.between(0, 1) < this.chanceToShoot)
                return;
            var bulletCount = 1;

            if (this.enemyType == EnemyType.Green) {
                bulletCount = 3;
            }

            if (this.enemyType == EnemyType.BigPurple) {
                bulletCount = 5;
            }

            for (var i = 0; i < bulletCount; i++)
                this.fireBullet();

        }

        fireBullet() {
            var bullet = new PlayerBullet(this.gameManager, false);
            var point = this.gameManager.getPlayerPosition();
            point.x = point.x + this.game.rnd.integerInRange(-this.aimRandomOffset, this.aimRandomOffset);
            point.y = point.y + this.game.rnd.integerInRange(-this.aimRandomOffset, this.aimRandomOffset);

            var dir = new Phaser.Point(point.x - this.body.x, point.y - this.body.y);
            var dirNormalise = new Phaser.Point();
            Phaser.Point.normalize(dir, dirNormalise);
            var vel = new Phaser.Point(dirNormalise.x * this.bulletSpeed, dirNormalise.y * this.bulletSpeed);
            bullet.setPositionVelocity(this.body.position, vel);
            if (this.gameManager.SoundOn)
                this.shootSound.play();
        }

        setBezierCurveMovement(pathPointsx: Array<Array<number>>, pathPointsy: Array<Array<number>>, curveTime: number) {
            this.pathPointsx = pathPointsx;
            this.pathPointsy = pathPointsy;


            this.startedCurveMovement = true;
            this.currentSubFragmentIndex = 0;
            this.pathCurrentTime = 0;
            this.currentPerc = 0;
            this.curveTime = curveTime;
        }

        destroy(remove: boolean) {
            if (remove)
                this.gameManager.removeEnemy(this);
            this.timer.removeAll();
            this.sprite.kill();
        }

    }

}