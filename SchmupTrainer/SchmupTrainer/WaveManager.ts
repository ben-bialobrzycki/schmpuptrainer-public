﻿module myGame {
    export class WaveManager {

        gameManager: GameManager;
        game: Phaser.Game;
        waveTimeInterval = 3000; //ms
        timeToNextWave;
        
        currentWave: number;
        wavesForLevel: number;
        waveTimer: Phaser.Timer;

        constructor(gameManager: GameManager) {
            this.gameManager = gameManager;
            this.game = this.gameManager.game;
            this.pathCollection = new PresetPathCollection();
            this.pathCollection.generatePaths(this.game.width, this.game.height);
        //    this.waveTimer = this.game.time.create(false);
        }

        startWaves() {
            this.timeToNextWave = 0;
        }

        setWavesForLevel() {
            //this.wavesForLevel = 7;
            //this.currentWave = this.wavesForLevel;
            this.setEventsForLevel();

        }


        setEventsForLevel() {
            this.currentWave = 0;
            var time = 1000;
            this.waveTimer = this.gameManager.levelEventTimer;
            if (this.gameManager.currentLevel == 1) {
                this.waveTimer.add(time, () => this.addWave());
                this.waveTimer.add(time += 3000, () => this.addSingleLinearWaveByName(0,0,PatternName.One, false, false,0.8));
                this.waveTimer.add(time += 3000, () => this.addSingleLinearWaveByName(0, 0, PatternName.Two, false, false,0.8));
                this.waveTimer.add(time += 2000, () => this.addSingleEnemySinus(EnemyType.Green, PatternName.Three, 75, 0.005));



             //   this.waveTimer.add(time += 3000, () => this.addSingleLinearWave(4, false, false));
                //this.waveTimer.add(time += 3000, () => this.addSingleLinearWave(1, false, false));
                //this.waveTimer.add(time += 3000, () => this.addGreenEnemySinus());
                //this.waveTimer.add(time += 3000, () => this.addWave());
            }
            if (this.gameManager.currentLevel == 2) {

                this.waveTimer.add(time += 3000, () => this.addSingleLinearWaveByName(-0.2, 0, PatternName.One, false, true,1.1));
                this.waveTimer.add(time, () => this.addSingleLinearWaveByName(0.2, 0, PatternName.One, true, true,1.1));
                this.waveTimer.add(time += 4000, () => this.addSingleLinearWaveByName(0, 0, PatternName.Three, false, false));
                this.waveTimer.add(time += 2000, () => this.addSingleEnemySinus(EnemyType.Green, PatternName.SnakeDown, 50, 0.02));
                this.waveTimer.add(time += 3000, () => this.addSingleLinearWaveByName(0, 0, PatternName.Two, false, false, 0.8));
                this.waveTimer.add(time += 2000, () => this.addSingleLinearWaveByName(-0.1, 0, PatternName.Two, false, false, 0.8));
            }
            if (this.gameManager.currentLevel == 3) {

                this.waveTimer.add(time += 3000, () => this.addSingleLinearWaveByName(-0.2, 0, PatternName.Four, false, true));
                this.waveTimer.add(time, () => this.addSingleLinearWaveByName(0.2, 0, PatternName.Four, true, true));
                this.waveTimer.add(time += 5000, () => this.addSingleLinearWaveByName(0, 0.1, PatternName.Three, false, false));
                this.waveTimer.add(time , () => this.addSingleLinearWaveByName(0, -0.1, PatternName.Three, true, false));
            //    this.waveTimer.add(time += 1000, () => this.addWave());
                this.waveTimer.add(time += 1000, () => this.addSingleEnemySinus(EnemyType.Green, PatternName.SnakeDown, 50, 0.02));
                this.waveTimer.add(time += 3000, () => this.addBezierWaveByName(PatternName.SnakeDown, 3000, 0.1, 0, false, false));
                this.waveTimer.add(time += 3000, () => this.addBezierWaveByName(PatternName.BezSide, 3000, 0, 0, true, false));
            }
            if (this.gameManager.currentLevel == 4) {

                this.waveTimer.add(time += 3000, () => this.addBezierWaveByName(PatternName.SnakeDown, 3000, 0, 0, false, false));
                this.waveTimer.add(time, () => this.addSingleLinearWaveByName(0.2, 0, PatternName.Four, true, true));
                this.waveTimer.add(time += 4000, () => this.addSingleLinearWaveByName(0.1, 0, PatternName.Three, false, false));
                this.waveTimer.add(time, () => this.addSingleLinearWaveByName(-0.1, 0, PatternName.Three, true, false));

                this.waveTimer.add(time += 4000, () => this.addSingleLinearWaveByName(0.1, 0, PatternName.Three, false, true));
                this.waveTimer.add(time, () => this.addSingleLinearWaveByName(-0.1, 0, PatternName.Three, true, true));
                this.waveTimer.add(time += 1000, () => this.addSingleEnemySinus(EnemyType.Green, PatternName.SnakeDown, 50, 0.02));
                this.waveTimer.add(time += 3000, () => this.addBezierWaveByName(PatternName.BezSide, 3000, 0, 0, false, false));
            }
            if (this.gameManager.currentLevel == 5) {
                this.waveTimer.add(time+=1000, () => this.addSingleEnemySinus(EnemyType.BigPurple, PatternName.Two, 100, 0.015));
                this.waveTimer.add(time += 3000, () => this.addBezierWaveByName(PatternName.SnakeDown, 2500, 0, 0, false, false));
                this.waveTimer.add(time += 2000, () => this.addSingleLinearWaveByName(0.2, 0, PatternName.Four, true, true));
                this.waveTimer.add(time += 1000, () => this.addSingleEnemySinus(EnemyType.BigPurple, PatternName.SnakeDown, 150, 0.010));
                this.waveTimer.add(time, () => this.addSingleLinearWaveByName(-0.2, 0, PatternName.Four, false, true));

                this.waveTimer.add(time += 2000, () => this.addSingleLinearWaveByName(0.2, 0, PatternName.Five, true, true));
                this.waveTimer.add(time, () => this.addSingleLinearWaveByName(-0.2, 0, PatternName.Five, false, true));
                this.waveTimer.add(time += 1000, () => this.addWave());
                this.waveTimer.add(time += 2500, () => this.addBezierWaveByName(PatternName.BezSide, 2500, 0, 0, false, false));
               
              
            }
        }

        addSingleEnemySinus(enemyType: EnemyType, pattern: PatternName, radius: number, angularVelocity: number) {

            var enemy = new Enemy(this.gameManager, this.game, enemyType);
            //enemy.setPosition(position);
            var paths = this.pathCollection.pathDict[pattern.toString()];
            var pathOption = new PresetPathOptions();
            pathOption.radius = radius;
            pathOption.angularVelocity = angularVelocity;
            enemy.setSinusoidalPath(paths, pathOption);
            this.gameManager.amendTotalPossibleScoreForEnemy(enemy);
        }

        pathCollection: PresetPathCollection;

        addOnePresetPathEnemy(path: PresetPath, pathOptions: PresetPathOptions, speed:number, wave:Wave) {
            var enemy = new Enemy(this.gameManager, this.game, EnemyType.blue);
            enemy.wave = wave;
            enemy.setPresetPath(path, pathOptions);
            if (speed )
                enemy.velocity = enemy.velocity  * speed;
            this.gameManager.amendTotalPossibleScoreForEnemy(enemy);
        }

        //DEBUG for testing
        addLinearWave() {

            this.addSingleEnemySinus(EnemyType.Green, PatternName.SnakeDown, 100, 0.02);
        }
        
        
        addSingleLinearWaveByName(xOffset, yOffset, pattern: PatternName, mirrorX: boolean, mirrorY: boolean,speed:number = 1) {
            var path = this.pathCollection.pathDict[pattern.toString()];
            var len = this.game.rnd.integerInRange(3, 5);
            var pathOptions = new PresetPathOptions();
            pathOptions.offset.x = xOffset;
            pathOptions.offset.y = yOffset;
            pathOptions.mirrorY = mirrorY;
            pathOptions.mirrorX = mirrorX;
            var wave = new Wave();
            wave.waveEnemyCount = len;
            this.gameManager.amendTotalPossibleScoreForWave(wave);
            for (var i = 0; i < len; i++) {
                this.gameManager.levelEventTimer.add(800 * i, () => { this.addOnePresetPathEnemy(path, pathOptions, speed,wave) });
            }
        }


        addBezierWaveByName(pattern: PatternName, curveTime: number, offsetX: number, offsetY: number, mirrorX: boolean, mirrorY: boolean) {

            var prePath = this.pathCollection.pathDict[pattern.toString()];
            var xPathCollection = new Array<Array<number>>();
            var yPathCollection = new Array<Array<number>>();
            var path: PathSet = new PathSet();
            for (var i = 0; i < prePath.parts.length; i++) {
                var part = prePath.parts[i];
                var xArray = new Array<number>();
                var yArray = new Array<number>();
                if (i > 0) {
                    var prevx = xPathCollection[i - 1];
                    xArray.push(prevx[prevx.length - 1]);
                    var prevy = yPathCollection[i - 1];
                    yArray.push(prevy[prevy.length - 1]);
                }
                for (var j = 0; j < part.points.length; j++) {

                    var x = part.points[j].x;
                    var y = part.points[j].y;
                    if (mirrorX)
                        x = this.gameManager.calculateMirroredX(x);
                    if (mirrorY)
                        y = this.gameManager.calculateMirroredY(y);
                    x = this.gameManager.applyOffsetX(x, offsetX);
                    y = this.gameManager.applyOffsetY(y, offsetY);
                    xArray.push(x);
                    yArray.push(y);
                }
                xPathCollection.push(xArray);
                yPathCollection.push(yArray);
            }
            var path = PathSet.generateRandomForPointCount(this.gameManager, this.game.rnd.integerInRange(3, 6), null, true);
            var len = this.game.rnd.integerInRange(8, 12);
            var wave = new Wave();
            wave.waveEnemyCount = len;
            for (var i = 0; i < len; i++) {
                this.gameManager.levelEventTimer.add(800 * i, () => { this.addOneBezierEnemy(new Phaser.Point(0, 0), xPathCollection, yPathCollection, curveTime, wave) });
            }
        }


        addWave() {

            var enemyCount = 5;
            var wave = new Wave();
            wave.waveEnemyCount = enemyCount;
            this.gameManager.amendTotalPossibleScoreForWave(wave);
            for (var i = 0; i < enemyCount; i++) {
                var enemy = new Enemy(this.gameManager, this.game, EnemyType.Yellow);
                enemy.wave = wave;
                var startPos: Phaser.Point = new Phaser.Point(this.game.rnd.integerInRange(this.gameManager.width * 0.10, this.gameManager.width * 0.90), -20);
                var velocity = new Phaser.Point(this.game.rnd.integerInRange(-Enemy.RandomXSpeed, Enemy.RandomXSpeed),
                    this.game.rnd.integerInRange(Enemy.RandomYSpeedMin, Enemy.RandomYSpeedMax));
                enemy.setPosition(startPos);
                enemy.setVelocity(velocity);

                this.gameManager.amendTotalPossibleScoreForEnemy(enemy);
            }
        }

        addOneBezierEnemy(position: Phaser.Point, pathPointsx: Array<Array<number>>, pathPointsy: Array<Array<number>>, curveTime:number, wave:Wave) {
            var enemy = new Enemy(this.gameManager, this.game, EnemyType.blue);
            enemy.wave = wave;
            enemy.setPosition(position);
            enemy.setBezierCurveMovement(pathPointsx, pathPointsy, curveTime);
            this.gameManager.amendTotalPossibleScoreForEnemy(enemy);
        }

    }
}