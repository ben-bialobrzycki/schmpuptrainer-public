﻿module myGame {
    export class BackgroundScroller {

        backgroundable: IBackgroundable;
        game: Phaser.Game;
        constructor(backgroundable: IBackgroundable, game: Phaser.Game) {
            this.backgroundable = backgroundable;
            this.game = game;
        }

        initialiseBackround() {

            var galaxycount = 2;
            var star1count = 15;
            var star2count = 10;

            for (var i = 0; i < galaxycount; i++) {
                var bi: BackgroundItem = new BackgroundItem(this.backgroundable, this.game, BackgroundSpriteType.galaxy)
                bi.setInitial();
            }
            for (var i = 0; i < star1count; i++) {
                var bi: BackgroundItem = new BackgroundItem(this.backgroundable, this.game, BackgroundSpriteType.star1)
                bi.setInitial();
            }
            for (var i = 0; i < star2count; i++) {
                var bi: BackgroundItem = new BackgroundItem(this.backgroundable, this.game, BackgroundSpriteType.star2)
                bi.setInitial();
            }
            this.game.world.sendToBack(this.backgroundable.backgroundGroup);
        }
    }
}