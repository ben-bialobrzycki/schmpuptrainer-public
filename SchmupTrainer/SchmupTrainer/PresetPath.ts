﻿module myGame {

    export enum PathType { linear, curve };
    export class PresetPathOptions {
        mirrorX: boolean;
        mirrorY: boolean;
        offset: PathPoint;
        radius: number;
        angularVelocity: number;

        constructor() {
            this.mirrorX = false;
            this.mirrorY = false;
            this.offset = { x: 0, y: 0 };
            this.radius = 0;
            this.angularVelocity = 0;
        }
    }
    export interface PresetPath {

        parts: Array<PathFragment>;

    }


    export interface PathFragment {

        //totalDistance: number;
    
        type: PathType;
        //xpoints: Array<number>;
        //ypoints: Array<number>;
        points: Array<PathPoint>;
      
    }
    export interface PathPoint {
        x: number;
        y: number;
    }

    export enum PatternName { "One", "Two", "Three", "Four", "Five", "SnakeDown" , "BezSide"};
    export class PresetPathCollection {

        paths: Array<PresetPath>;
        pathDict: { [id: string]: PresetPath } = {};

        generatePaths(width, height) {
            this.paths = new Array<PresetPath>();
            this.pathDict = {};
            this.setAllPatterns(width, height);
            //this.paths.push(this.getPattern1());
            //this.paths.push(this.getPattern2());
            //this.paths.push(this.getPattern3());
            //this.paths.push(this.getPattern4());
            //this.paths.push(this.getPattern5());

      //      this.convertScreenRatiosToActualPositions(width, height);
          

            //this.pathDict[PatternName.One.toString()] = this.paths[0];
            //this.pathDict[PatternName.Two.toString()] = this.paths[1];
            //this.pathDict[PatternName.Three.toString()] = this.paths[2];
            //this.pathDict[PatternName.Four.toString()] = this.paths[3];
            //this.pathDict[PatternName.Five.toString()] = this.paths[4];
        }

        convertScreenRatiosToActualPositions(width,height) {
            for (var i = 0; i < this.paths.length; i++) {
                this.convertScreenRatiosToActualPositionsForPath(width,height,this.paths[i]);
                //for (var j = 0; j < this.paths[i].parts.length; j++) {
                //    for (var k = 0; k < this.paths[i].parts[j].points.length; k++) {
                //        this.paths[i].parts[j].points[k].x *= width;
                //        this.paths[i].parts[j].points[k].y *= height;
                //    }

                //}
            }
        }

        convertScreenRatiosToActualPositionsForPath(width, height, path: PresetPath) {
            
            for (var j = 0; j < path.parts.length; j++) {
                    for (var k = 0; k < path.parts[j].points.length; k++) {
                        path.parts[j].points[k].x *= width;
                        path.parts[j].points[k].y *= height;
                    }

                }
            
        }

        addPath(name: PatternName, width, height, path: PresetPath) {
            this.convertScreenRatiosToActualPositionsForPath(width, height, path);
            this.pathDict[name.toString()] = path;

        }
        setAllPatterns(width, height) {
            this.addPath(PatternName.One, width, height, {
                parts:
                [{
                    points: [{ x: 0.2, y: -0.05 }, { x: 0.5, y: 0.7 }, { x: 0.55, y: 0.75 }, { x: 0.7, y: 0.8 }, { x: 0.8, y: 0.3 }, { x: 0.85, y: 0.7 }, { x: 1, y: 1.3 }],
                    type: PathType.linear
                }]
            });

            this.addPath(PatternName.Two, width, height, 
                 {
                    parts:
                    [{
                        points: [{ x: -0.05, y: 0.7 }, { x: 0.2, y: 0.4 }, { x: 0.3, y: 0.3 }, { x: 0.4, y: 0.2 }, { x: 0.5, y: 0.5 }, { x: 0.6, y: 0.6 }, { x: 0.7, y: 0.7 }, { x: 0.9, y: 0.5 }, { x: 0.8, y: 0.4 }
                             , { x: 0.7, y: 0.3 }, { x: 0.6, y: 0.4 }, { x: 0.5, y: 0.5 }, { x: 0.4, y: 0.7 }, { x: 0.3, y: 0.9 }, { x: 0.2, y: 1.5 }
                            ],
                            type: PathType.linear
                        }]
                });

            this.addPath(PatternName.Three, width, height, 
                    {
                        parts:
                        [{
                                points: [{ x: -0.05, y: 0.7 }, { x: 0.4, y: 0.3 }, { x: 0.6, y: 0.5 }, { x: 0.9, y: 0.8 }, { x: 1.4, y: 0.3 }],
                                type: PathType.linear
                         }]
                    });

            this.addPath(PatternName.Four, width, height, 
                    {
                        parts:
                        [{
                                points: [{ x: -0.1, y: 0.3 }, { x: 0.5, y: 0.8 }, { x: 0.9, y: 0.2 }],
                                type: PathType.linear
                            }]
                    });
                
            

              this.addPath(PatternName.Five, width, height, 
                    {
                        parts:
                        [{
                                points: [{ x: -0.1, y: 0.3 }, { x: 0.5, y: 0.8 }, { x: 0.9, y: 0.2 }, { x: 1.4, y: 0.7 }],
                                type: PathType.curve
                            }]
                });

            this.addPath(PatternName.SnakeDown, width, height,
                  {
                      parts:
                      [
                          {points: [{ x: -0.1, y: -0.1 }, { x: 0.2, y: 0.2 }, { x: 0.9, y: 0.3 }],type: PathType.curve},
                          {points: [ { x: 0.5, y: 0.3 }, { x: 0.1, y: 0.5 }],type: PathType.curve},
                          {points: [{ x: 0.5, y: 0.5 }, { x: 0.9, y: 0.6 }],type: PathType.curve}
                          ,{points: [ { x: 0.5, y: 0.6 }, { x: 0.1, y: 0.75 }],type: PathType.curve}
                          , {points: [ { x: 0.5, y: 0.7 }, { x: 1.4, y: 0.9 }],type: PathType.curve}
                      ]
                  });

            this.addPath(PatternName.BezSide, width, height,
                {
                    parts:
                    [
                        { points: [{ x: -0.1, y: 0.6 }, { x: 0.4, y: 0.3 }, { x: 0.9, y: 0.1 }], type: PathType.curve },
                        { points: [{ x: 0.6, y: 0.4 }, { x: 0.1, y: 0.1 }], type: PathType.curve },
                        { points: [{ x: 0.4, y: 0.3 }, { x: 0.6, y: 0.7 },{ x: 0.9, y: 0.1 }], type: PathType.curve }
                        , { points: [{ x: 0.6, y: 0.4 }, { x: -0.2, y: 1.1 }], type: PathType.curve }
                    ]
                });
        }
      
       
    }

}