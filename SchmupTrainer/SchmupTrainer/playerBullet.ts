﻿module myGame {

    export class PlayerBullet implements IUpdateable {
        gameManager: GameManager;

        playerCollisionScoreCost = 0;//80; //using lives mechanic instead
        sprite: Phaser.Sprite;
        body: Phaser.Physics.Arcade.Body;
        active: boolean;
        playerBullet: boolean; //true for player projectiles false for enemy ones
        damageValue = 1;

        constructor(gameManager: GameManager, fromPlayer: boolean) {
            this.gameManager = gameManager;
            this.playerBullet = fromPlayer;

            this.active = true;
            if (fromPlayer)
                this.sprite = this.gameManager.game.add.sprite(0, 0, 'playerBullet');
            else {
                this.sprite = this.gameManager.game.add.sprite(0, 0, 'EnemyBullet');
              
            }
            this.sprite.animations.add('move', [0, 1], 10, true);
            this.sprite.animations.play('move');
            this.sprite.scale.setTo(GameManager.SCALE, GameManager.SCALE);
            this.gameManager.game.physics.arcade.enable(this.sprite);
            if (fromPlayer)
                this.gameManager.playerBulletGroup.add(this.sprite);
            else
                this.gameManager.enemyBulletGroup.add(this.sprite);

            this.body = this.sprite.body;
            this.sprite.data = this;
            this.sprite.events.onOutOfBounds.add((sprite) => { sprite.kill(); })
            this.gameManager.addToUpdateables(this);

        }

        setPositionVelocity(position: Phaser.Point, velocity: Phaser.Point) {
            this.sprite.position.x = position.x;
            this.sprite.position.y = position.y;
            this.sprite.body.x = position.x;
            this.sprite.body.y = position.y;
            this.sprite.body.velocity.x = velocity.x;
            this.sprite.body.velocity.y = velocity.y;
        }

        update()
        {
            this.sprite.update();
        }

        isActive() {
            return this.active;
        }
    }
}