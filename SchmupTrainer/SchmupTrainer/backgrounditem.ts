﻿
module myGame {

    export interface IBackgroundable {
        width: number;
        height: number;
        backgroundGroup;
        addToUpdateables(item: BackgroundItem);
    }

    export enum BackgroundSpriteType { star1, star2, galaxy }
    export class BackgroundItem implements IUpdateable {

        sprite: Phaser.Sprite;
        game: Phaser.Game;
        gameManager: IBackgroundable;
        speed: number;
        alive: boolean;
        body: Phaser.Physics.Arcade.Body;

        minYSpeed = 10;
        maxYSpeed = 40;

        backgroundItemType: BackgroundSpriteType;

        constructor(gameManager: IBackgroundable, game: Phaser.Game, backgroundType: BackgroundSpriteType) {
            this.backgroundItemType = backgroundType;
            this.gameManager = gameManager;
            this.game = game;
            this.setParametersForType();
            this.sprite.scale.setTo(4, 4);

            this.game.physics.arcade.enable(this.sprite);

            this.sprite.data = this;

            this.body = this.sprite.body;
            this.gameManager.addToUpdateables(this);
            this.gameManager.backgroundGroup.add(this.sprite);
        }

        setParametersForType() {
            if (this.backgroundItemType == BackgroundSpriteType.star1) {
                this.sprite = this.game.add.sprite(0, 0, 'star1');
            }
            else if (this.backgroundItemType == BackgroundSpriteType.star2) {
                this.sprite = this.game.add.sprite(0, 0, 'star2');
            }
            else if (this.backgroundItemType == BackgroundSpriteType.galaxy) {
                this.sprite = this.game.add.sprite(0, 0, 'galaxy');

            }
            this.sprite.z = -1;
            this.sprite.blendMode = PIXI.blendModes.NORMAL;

        }

        resetTint() {
            this.sprite.tint = 0xffffff;
        }

        setPosition(position: Phaser.Point) {
            this.sprite.position.x = position.x;
            this.sprite.position.y = position.y;
            this.body.position.x = position.x;
            this.body.position.y = position.y;

        }

        setVelocity(velocity: Phaser.Point) {
            this.body.velocity.x = velocity.x;
            this.body.velocity.y = velocity.y;
        }


        update() {

            if (this.body.position.y > this.gameManager.height + 50) {
                var x = this.game.rnd.integerInRange(0, this.gameManager.width);
                var y = -20;

                this.setPosition(new Phaser.Point(x, y));
            }
        }

        isActive() {
            return true;
        }

        setInitial() {
            var x = this.game.rnd.integerInRange(0, this.gameManager.width);
            var y = this.game.rnd.integerInRange(0, this.gameManager.height);

            this.setPosition(new Phaser.Point(x, y));
            this.setRandomVelocity();
        }

        setRandomVelocity() {
            var yvel = this.game.rnd.integerInRange(this.minYSpeed, this.maxYSpeed);
            this.setVelocity(new Phaser.Point(0, yvel));
        }

        destroy() {
            this.sprite.kill();
        }
    }
}