﻿module myGame {
    export class Explosion {

        sprite: Phaser.Sprite;
        game: Phaser.Game;
        gameManager: GameManager;
        sound: Phaser.Sound;

        alive: boolean;
        static LifeTime = 2000;

        constructor(gameManager: GameManager) {
            //this.sprite = sprite;

            this.gameManager = gameManager;
            this.game = this.gameManager.game;
            this.sprite = this.game.add.sprite(0, 0, 'explosion');
            this.sound = this.game.add.audio('Explosion');
            this.sprite.scale.setTo(GameManager.SCALE, GameManager.SCALE);


            this.sprite.animations.add('run', [0, 1, 2, 3, 4, 5], 10, false);
            this.sprite.data = this;

        }

        setPosition(position: Phaser.Point) {
            this.sprite.position.x = position.x;
            this.sprite.position.y = position.y;

            this.sprite.animations.play("run");
            this.alive = true;
            var timer = this.game.time.create();
            timer.add(Explosion.LifeTime, () => { this.finish(); })
            if (this.gameManager.SoundOn)
                this.sound.play();

        }

        finish() {
            this.alive = false;
        }

        update() {

        }
    }
}