﻿/// <reference path="typing/phaser.d.ts" />
/// <reference path="gamemanager.ts" />

module myGame {


    window.onload = () => {

        var width = 800;
        var height = 600;
        var parentElement = document.getElementById('content');
        var fullGame = new Phaser.Game(width, height, Phaser.AUTO, 'gameContainer', parentElement, false, false);
        var simpleGame = new GameManager(fullGame);
        var menu = new MainMenu(fullGame);

        var btn = document.getElementById('btnUploadScore');


        btn.addEventListener('click', () => simpleGame.submitScore());
        var btnSkip = document.getElementById('btnSkipUpload');
        btnSkip.addEventListener('click', () => simpleGame.skipUpload());

        fullGame.state.add("Game", simpleGame);
        fullGame.state.add("MainMenu", menu);
        fullGame.state.start("MainMenu");
        // fullGame.state.start("Game");

    };

}