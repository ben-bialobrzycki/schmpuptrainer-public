﻿module myGame {

    interface IScoreDollarOne{
        name: string;
        score: number;

    }

    export interface IScore
    {
        id: number;
        scorePoints: number;
        scorePlayerName: string;
        createDate: string;
        scoreBoardAPIKey: string;
    }

    export interface IScoreBoard
    {
        scores: Array<IScore>;
        scoreBoardName: string;
    }
    export class MainMenu implements IBackgroundable {

        fkey: Phaser.Key;

        width: number;
        height: number;
        backgroundGroup: Phaser.Group;
        updateables: Array<IUpdateable>;
        fullGame: Phaser.Game;
        backgroundScroller: BackgroundScroller
        constructor(fullGame) {
            this.fullGame = fullGame;
        }

        addToUpdateables(updateable: IUpdateable) {
            this.updateables.push(updateable);
        }
        preload() {
            this.fullGame.load.image('menubackground', 'assets/Title.png');
            this.fullGame.load.image('MenuLogo', 'assets/TitleLogo.png');
          //  this.fullGame.load.image('PlayButton', 'assets/MenuPlayButton.png');
            this.fullGame.load.image('PlayButton', 'assets/PlayButton.png');
            this.fullGame.load.image('FullScreenButton', 'assets/FullScreenButton.png');
           
            this.fullGame.load.image('star1', 'assets/star1.png');
            this.fullGame.load.image('star2', 'assets/star2.png');
            this.fullGame.load.image('galaxy', 'assets/galaxy.png');
        }

      
        scoreBoardText: Phaser.Text;
        create() {
    
            var background = this.fullGame.add.image(100, 100, "MenuLogo"); 
            background.z = -10;
            
            this.width = this.fullGame.width;
            this.height = this.fullGame.height;
            this.fkey = this.fullGame.input.keyboard.addKey(GameManager.KEYF);
            this.fullGame.stage.backgroundColor = '#111111';

            // Stretch to fill
            this.fullGame.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
            var buttonPlay = this.fullGame.add.button(500, 400, 'PlayButton', () => this.startGame());
         //  buttonPlay.scale = new Phaser.Point(1.5, 1.5);
            GameManager.addFullScreenButton(this.fullGame);

            var styleScoreText = { font: "15px 'Lucida Console', Monaco, monospace", fill: "#ffffff", align: "centre", boundsAlignH: "center", boundsAlignV: "middle" };
            this.scoreBoardText = this.fullGame.add.text(0, 0, "High Scores", styleScoreText);
            this.scoreBoardText.fixedToCamera = true;

            this.scoreBoardText.setTextBounds(10, 150, 400, 600);
         
            GameManager.getHighScores((response) => { this.showHighScores(response); })

            this.updateables = new Array<IUpdateable>();
            this.backgroundGroup = this.fullGame.add.group();
            this.backgroundScroller = new BackgroundScroller(this, this.fullGame);
            this.backgroundScroller.initialiseBackround();
        }

        //lazy string padding
        RightPadString(s: string, len: number) {
            var ns = s;
            while (ns.length < len) {
                ns = ns + ' ';
            }
            return ns;
        }

        maxScoreNumber = 12;
        //showHighScores(response) {
        //    var jsonObject: Array<IScoreDollarOne> = JSON.parse(response);
        //    var fullString = "High Scores:";
        //    jsonObject.sort((s1, s2) => { return s2.score - s1.score; })
        //    for (var i = 0; i < jsonObject.length && i < this.maxScoreNumber; i++) {
        //        fullString += "\n" + this.RightPadString(jsonObject[i].name,25) + "       " + jsonObject[i].score;
        //    }
        //    this.scoreBoardText.text = fullString;
        //}

        showHighScores(response) {
            var jsonObject: IScoreBoard = JSON.parse(response);
            var fullString = "High Scores:";
            jsonObject.scores.sort((s1, s2) => { return s2.scorePoints - s1.scorePoints; })
            for (var i = 0; i < jsonObject.scores.length && i < this.maxScoreNumber; i++) {
                fullString += "\n" + this.RightPadString(jsonObject.scores[i].scorePlayerName, 25) + "       " + jsonObject.scores[i].scorePoints;
            }
            this.scoreBoardText.text = fullString;
        }


        update() {
            if (this.fkey.justDown)
                GameManager.gofull(this.fullGame);
            this.updateUpdateables();
        }

        updateUpdateables() {
            for (var i = 0; i < this.updateables.length; i++) {
                this.updateables[i].update();
            }
        }

        startGame() {
            this.fullGame.state.start("Game",true,true);
        }
    }
}