﻿module myGame {
    export class Player {

        game: Phaser.Game
        gameManager: GameManager;
        sprite: Phaser.Sprite;
        cursors: Phaser.CursorKeys;
        fireResetTime;
        defaultFireResetTime = 300.0;
        fireReset = 0;
        bulletSpeed = 400;
        playerSpeed = 250;
        shootSound: Phaser.Sound;
        pad1: Phaser.SinglePad;
        velocity: Phaser.Point;
        bulletCost = 20;
        body: Phaser.Physics.Arcade.Body;
        isDead: boolean;
        playerStart: Phaser.Point;

        keyW: Phaser.Key;
        keyA: Phaser.Key;
        keyS: Phaser.Key;
        keyD: Phaser.Key;

        constructor(gameManager: GameManager, phaserGame: Phaser.Game) {
            this.game = phaserGame;
            this.gameManager = gameManager;
            this.cursors = this.game.input.keyboard.createCursorKeys();

            
            this.pad1 = this.game.input.gamepad.pad1;
            this.velocity = new Phaser.Point(0, 0);
            this.shootSound = this.game.add.audio('PlayerShoot');
            this.playerStart = new Phaser.Point(this.gameManager.width / 2, this.game.world.height - 128);
            this.sprite = this.game.add.sprite(this.playerStart.x, this.playerStart.y, 'ship');
            this.sprite.animations.add('move', [0, 1], 10, true);
            this.sprite.scale.setTo(GameManager.SCALE, GameManager.SCALE);
            
            this.game.physics.arcade.enable(this.sprite);
            this.body = this.sprite.body;
            
            this.fireResetTime = this.defaultFireResetTime;
            this.sprite.animations.play('move');
            this.timer = this.game.time.create(false);
            this.timer.start();
            this.sprite.blendMode = PIXI.blendModes.NORMAL;
            this.keyW = this.game.input.keyboard.addKey(87);
            this.keyA = this.game.input.keyboard.addKey(65);
            this.keyS = this.game.input.keyboard.addKey(83);
            this.keyD = this.game.input.keyboard.addKey(68);
            this.resetForNewGame();
        }


        update() {
            if (this.fireReset > 0)
                this.fireReset -= this.game.time.elapsedMS;
            this.getInput();
        }

        StopMovement() {
            this.body.velocity.x = this.body.velocity.y = 0; 
        }

        //getGamePadInput() {
        //    // Controls
        //    if (this.pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
        //        sprite.x--;
        //    }
        //    else if (pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
        //        sprite.x++;
        //    }

        //    if (pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_UP) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) < -0.1) {
        //        sprite.y--;
        //    }
        //    else if (pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) > 0.1) {
        //        sprite.y++;
        //    }

        //    if (pad1.justPressed(Phaser.Gamepad.XBOX360_A)) {
        //        sprite.angle += 5;
        //    }

        //}

        inputLeft() {
            return (this.cursors.left.isDown || this.keyA.isDown
               || (this.pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1))
                && this.sprite.body.position.x > 0.05 * this.game.width;
        }

        inputRight() {
            return (this.cursors.right.isDown || this.keyD.isDown
                || this.pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1)
                && this.sprite.body.position.x < 0.95 * this.game.width;
        }

        inputUp() {
            return (this.cursors.up.isDown || this.keyW.isDown ||  this.pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_UP) || this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) < -0.1) && this.sprite.body.position.y > 0.05 * this.game.height;
        }

        inputDown() {
            return (this.cursors.down.isDown || this.keyS.isDown || this.pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN) || this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) > 0.1) && this.sprite.body.position.y < 0.95 * this.game.height;
        }
        getInput() {

            this.sprite.body.velocity.x = 0;
            this.sprite.body.velocity.y = 0;
    
            if (this.inputLeft()) {
                this.sprite.body.velocity.x = -this.playerSpeed;
            }
            if (this.inputRight()) {
                this.sprite.body.velocity.x = this.playerSpeed;
            }
            if (this.inputUp()) {
                this.sprite.body.velocity.y = -this.playerSpeed;
            }
            if (this.inputDown()) {
                this.sprite.body.velocity.y = this.playerSpeed;
            }
            else {
                //  Stand still
            }

            var fireDown: boolean = this.game.input.keyboard.isDown(GameManager.FIREKEY) || this.pad1.isDown(Phaser.Gamepad.XBOX360_A);
            if (fireDown && this.fireReset <= 0) {
                this.doFire();
            }
           

        }

        bulletOffsetX = 8;
        bulletOffsetY = -6;
        doFire() {
            this.fireReset = this.fireResetTime;
            //TODO Pool bullets
            var bullet = new PlayerBullet(this.gameManager, true);
           var bulletStart = new Phaser.Point(this.body.position.x + this.bulletOffsetX, this.body.position.y + this.bulletOffsetY);
           bullet.setPositionVelocity(bulletStart, new Phaser.Point(0, -this.bulletSpeed));
           this.gameManager.updateScore(-this.bulletCost);
           if (this.gameManager.SoundOn)
                this.shootSound.play();
        }

        playerEnemyCollision(obj1: any, obj2: any) {
            var enemySprite: Phaser.Sprite = obj2;
            var enemy: Enemy = enemySprite.data;
            this.takeDamage();
            this.gameManager.addExplosion(enemy.body.position);
            enemy.destroy(true);
            this.gameManager.updateScore(-enemy.playerCollisionScoreCost);
        }

        timer: Phaser.Timer;
        flashTime = 300;
        immunityTime = 1000;
        resetTime = 500;
        immune: boolean = false;
        playerLives;
        initialPlayerLives = 3;
        takeDamage() {
            if (this.immune)
                return;
            this.gameManager.addExplosion(this.sprite.position);
            this.sprite.visible = false;
            this.immune = true;
            this.playerLives--;
            if (this.playerLives > 0) {
                this.timer.add(this.resetTime, () => { this.reset(); })
            }
            else {
                this.gameManager.runGameOver();
            }
            this.gameManager.updatePlayerLivesText();

        }

        isAlive() {
            return this.sprite.visible;
        }
        resetImmunity() {
            this.immune = false;
            this.sprite.tint = 0xffffff;
        }


        resetForNewGame() {
            this.reset();
            this.playerLives = this.initialPlayerLives;
        }
        reset() {
            this.sprite.visible = true;
            this.sprite.position.x = this.playerStart.x;
            this.sprite.position.y = this.playerStart.y;
            this.sprite.tint = 0xff00ff;
            this.timer.add(this.immunityTime, () => { this.resetImmunity(); })
        }

        resetTint() {
            this.sprite.tint = 0xffffff;
        }

        playerEnemyBulletCollision(obj1: any, obj2: any) {
            var enemySprite: Phaser.Sprite = obj2;
            enemySprite.kill();
            this.takeDamage();
            var enemy: PlayerBullet = enemySprite.data;
            this.gameManager.updateScore(-enemy.playerCollisionScoreCost);
        }

    }
}